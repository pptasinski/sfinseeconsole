<?php declare(strict_types=1);

namespace App\Console;

use App\Contract\BusInterface;
use App\Exception\EmptyRepositoryException;
use App\Exception\GeneralHandlerException;
use App\Exception\InvalidBranchNameException;
use App\Exception\InvalidRepositoryNameException;
use App\Exception\NotSupportedServiceException;
use App\Exception\RepositoryNotFoundException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class FindShaCommand
 * @package App\Command
 */
final class FindShaCommand extends Command
{

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var BusInterface
     */
    private $bus;

    /**
     * FindShaCommand constructor.
     * @param BusInterface $bus
     */
    public function __construct(BusInterface $bus)
    {
        $this->bus = $bus;
        parent::__construct('app:find-sha');

    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $availableServices = $this->bus->getSupportedServiceNames();
        $firstAvailableService = reset($availableServices);
        $this
            ->setDescription('Command finds sha sum of last commit in given branch')
            ->setHelp('If you want to find sha sum write app:find-sha repository branch service. Last parameter is optional.')
            ->addArgument('repository', InputArgument::REQUIRED, 'Repository name in format owner/repo')
            ->addArgument('branch', InputArgument::REQUIRED, 'Branch name')
            ->addOption('service', null, InputOption::VALUE_OPTIONAL, sprintf('Service name, default: %s', $firstAvailableService), $firstAvailableService);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        $repositoryName = $input->getArgument('repository');
        $branchName = $input->getArgument('branch');
        $serviceName = $input->getOption('service');
        if (isset($repositoryName, $branchName)) {
            return;
        }
        $this->io->title('Command Interactive Wizard');
        $this->io->text(['If you prefer to not use this interactive wizard, provide the', 'arguments required by this command as follows:', '', ' $ php bin/console app:find-sha repository branch --service=xxx (optional)', '', 'Now we\'ll ask you for the value of all the missing command arguments.',]);


        try{
            $this->validateServiceName($serviceName);
        } catch (NotSupportedServiceException $exception){
            $serviceName = $this->io->ask(sprintf('Service name %s is invalid. Please provide other name', $serviceName), null, [$this, 'validateServiceName']);
            $input->setOption('service', $serviceName);
        }

        if (null !== $repositoryName) {
            $this->io->text(' > <info>Repository name</info>: ' . $repositoryName);
        } else {
            $repositoryName = $this->io->ask('Repository name (format: owner/repo)', null, [$this, 'validateRepositoryName']);
            $input->setArgument('repository', $repositoryName);
        }
        if (null !== $branchName) {
            $this->io->text(' > <info>Branch name</info>: ' . $branchName);
        } else {
            $branchName = $this->io->ask('Branch name', null, [$this, 'validateBranchName']);
            $input->setArgument('branch', $branchName);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        try {
            $repositoryName = $this->validateRepositoryName($input->getArgument('repository'));
            $branchName = $this->validateBranchName($input->getArgument('branch'));
            $service = $this->validateServiceName($input->getOption('service'));
            $lastShaSum = $this->bus->getLastCommitShaSum($service, $repositoryName, $branchName);
            $this->io->success(sprintf('Last sha sum from branch %s in %s is %s.', $branchName, $repositoryName, $lastShaSum));
        } catch (RepositoryNotFoundException | EmptyRepositoryException | GeneralHandlerException | NotSupportedServiceException | InvalidBranchNameException | InvalidRepositoryNameException $exception) {
            $this->io->warning($exception->getMessage());
        }
        return 0;
    }

    /**
     * @param string|null $repositoryName
     * @return string
     */
    public function validateRepositoryName(?string $repositoryName): string
    {

        if (empty($repositoryName) || 1 !== preg_match('/[\S]\/[\S]/', $repositoryName)) {
            throw new InvalidRepositoryNameException((string)$repositoryName);
        }
        return $repositoryName;
    }

    /**
     * @param string|null $branchName
     * @return string
     */
    public function validateBranchName(?string $branchName): string
    {
        if (empty($branchName)) {
            throw new InvalidBranchNameException((string)$branchName);
        }
        return $branchName;
    }

    /**
     * @param string|null $serviceName
     * @return string
     */
    public function validateServiceName(?string $serviceName): string
    {
        $availableServices = $this->bus->getSupportedServiceNames();
        if (!in_array($serviceName, $availableServices, true)) {
            throw new NotSupportedServiceException((string)$serviceName);
        }
        return $serviceName;
    }
}
