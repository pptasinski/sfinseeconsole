<?php declare(strict_types=1);

namespace App\Bus;

use App\Contract\BusInterface;
use App\Exception\EmptyBusException;
use App\Exception\InvalidHandlerException;
use App\Exception\NotSupportedServiceException;
use App\Contract\HandlerInterface;

/**
 * Class HandlerBuss
 * @package App\Bus
 */
final class HandlerBus implements BusInterface
{
    /**
     * @var HandlerInterface[]
     */
    private $handlers;

    /**
     * HandlerBuss constructor.
     * @param iterable|HandlerInterface[] $handlers
     */
    public function __construct(iterable $handlers)
    {
        foreach ($handlers as $handler) {
            if (!$handler instanceof HandlerInterface) {
                throw new InvalidHandlerException();
            }
            $this->handlers[$handler->getSupportedServiceName()] = $handler;
        }

        if(!$this->handlers){
            throw new EmptyBusException(__CLASS__);
        }
    }


    /**
     * @inheritDoc
     */
    public function getLastCommitShaSum(string $serviceName, string $repositoryName, string $branchName): ?string
    {
        if (!array_key_exists($serviceName, $this->handlers)) {
            throw new NotSupportedServiceException($serviceName);
        }
        return $this->handlers[$serviceName]->getLastCommitShaSum($repositoryName, $branchName);
    }

    /**
     * @inheritDoc
     */
    public function getSupportedServiceNames(): array
    {
        return array_keys($this->handlers);
    }
}
