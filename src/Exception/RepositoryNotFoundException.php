<?php declare(strict_types=1);

namespace App\Exception;

use LogicException;

/**
 * Class RepositoryNotFoundException
 * @package App\Exception
 */
final class RepositoryNotFoundException extends LogicException
{
    /**
     * RepositoryNotFoundException constructor.
     * @param string $repositoryUrl
     */
    public function __construct(string $repositoryUrl)
    {
        parent::__construct(sprintf('Repository url %s not found!', $repositoryUrl));
    }
}
