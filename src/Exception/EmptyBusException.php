<?php declare(strict_types=1);

namespace App\Exception;

use LogicException;

/**
 * Class EmptyBusException
 * @package App\Exception
 */
final class EmptyBusException extends LogicException
{
    /**
     * EmptyBusException constructor.
     * @param string $className
     */
    public function __construct(string $className)
    {
        parent::__construct(sprintf('%s should have at least one handler!', $className));
    }
}
