<?php declare(strict_types=1);

namespace App\Exception;

use RuntimeException;
use Throwable;

/**
 * Class GeneralHandlerException
 * @package App\Exception
 */
final class GeneralHandlerException extends RuntimeException
{
    /**
     * GeneralHandlerException constructor.
     * @param Throwable|null $previous
     */
    public function __construct(Throwable $previous = null)
    {
        parent::__construct('General handler exception', 0, $previous);
    }
}
