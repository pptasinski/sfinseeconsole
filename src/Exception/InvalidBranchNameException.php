<?php declare(strict_types=1);

namespace App\Exception;

use InvalidArgumentException;

/**
 * Class InvalidBranchNameException
 * @package App\Exception
 */
final class InvalidBranchNameException extends InvalidArgumentException
{
    /**
     * InvalidBranchNameException constructor.
     * @param string $branchName
     */
    public function __construct(string $branchName)
    {
        parent::__construct(sprintf('Invalid branch name: %s.', $branchName));
    }

}
