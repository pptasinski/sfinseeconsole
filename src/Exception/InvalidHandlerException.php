<?php declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

/**
 * Class InvalidHandlerException
 * @package App\Exception
 */
final class InvalidHandlerException extends RuntimeException
{
    protected $message = 'Invalid handler instance.';
}
