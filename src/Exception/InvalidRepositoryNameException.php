<?php declare(strict_types=1);

namespace App\Exception;

use InvalidArgumentException;

/**
 * Class InvalidRepositoryNameException
 * @package App\Exception
 */
final class InvalidRepositoryNameException extends InvalidArgumentException
{
    /**
     * InvalidRepositoryNameException constructor.
     * @param string $repositoryName
     */
    public function __construct(string $repositoryName)
    {
        parent::__construct(sprintf('Invalid repository name: %s. It should match pattern: owner/repo.', $repositoryName));
    }

}
