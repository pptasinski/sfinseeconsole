<?php declare(strict_types=1);

namespace App\Exception;

use LogicException;

/**
 * Class EmptyRepositoryException
 * @package App\Exception
 */
final class EmptyRepositoryException extends LogicException
{
    /**
     * EmptyRepositoryException constructor.
     * @param string $repositoryName
     * @param string $branchName
     */
    public function __construct(string $repositoryName, string $branchName)
    {
        parent::__construct(sprintf('Branch %s in repository %s is empty!', $repositoryName, $branchName));
    }

}
