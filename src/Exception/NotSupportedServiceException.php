<?php declare(strict_types=1);

namespace App\Exception;

use LogicException;

/**
 * Class NotSupportedServiceException
 * @package App\Exception
 */
final class NotSupportedServiceException extends LogicException
{

    /**
     * NotSupportedServiceException constructor.
     * @param string $serviceName
     */
    public function __construct(string $serviceName)
    {
        parent::__construct(sprintf('Service %s is not supported at this moment.', $serviceName));
    }

}
