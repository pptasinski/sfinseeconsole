<?php declare(strict_types=1);

namespace App\Contract;

use App\Exception\EmptyRepositoryException;
use App\Exception\GeneralHandlerException;
use App\Exception\RepositoryNotFoundException;

/**
 * Interface HandlerInterface
 * @package App\Handler
 */
interface HandlerInterface
{
    /**
     * @param string $repositoryName
     * @param string $branchName
     * @return string|null
     */
    public function getLastCommitShaSum(string $repositoryName, string $branchName): ?string;

    /**
     * @return string
     * @throws RepositoryNotFoundException
     * @throws EmptyRepositoryException
     * @throws GeneralHandlerException
     */
    public function getSupportedServiceName(): string;
}
