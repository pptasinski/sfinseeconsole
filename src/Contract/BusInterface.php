<?php declare(strict_types=1);

namespace App\Contract;
/**
 * Interface BusInterface
 * @package App\Bus
 */
interface BusInterface
{
    /**
     * @param string $serviceName
     * @param string $repositoryName
     * @param string $branchName
     * @return string|null
     */
    public function getLastCommitShaSum(string $serviceName, string $repositoryName, string $branchName): ?string;

    /**
     * @return array|string[]
     */
    public function getSupportedServiceNames(): array;
}
