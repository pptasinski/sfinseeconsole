<?php declare(strict_types=1);

namespace App\Handler;

use App\Contract\HandlerInterface;
use App\Exception\EmptyRepositoryException;
use App\Exception\GeneralHandlerException;
use App\Exception\RepositoryNotFoundException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class GitHubHandler
 * @package App\Handler
 */
final class GitHubHandler implements HandlerInterface
{
    public const SERVICE_NAME = 'github';

    /**
     * @var HttpClientInterface
     */
    private $httpClient;


    /**
     * GitHubHandler constructor.
     */
    public function __construct()
    {
        $this->httpClient = HttpClient::create();
    }


    /**
     * @inheritDoc
     */
    public function getSupportedServiceName(): string
    {
        return self::SERVICE_NAME;
    }

    /**
     * @param string $repositoryName
     * @param string $branchName
     * @return string|null
     * @throws RepositoryNotFoundException
     * @throws EmptyRepositoryException
     * @throws GeneralHandlerException
     */
    public function getLastCommitShaSum(string $repositoryName, string $branchName): ?string
    {

        $repositoryUrl = sprintf('https://api.github.com/repos/%s/commits/%s', $repositoryName, $branchName);
        try {
            $request = $this->httpClient->request(Request::METHOD_GET, $repositoryUrl);
        } catch (TransportExceptionInterface $e) {
            throw new GeneralHandlerException($e);
        }

        if (Response::HTTP_OK !== $request->getStatusCode()) {
            throw new RepositoryNotFoundException($repositoryUrl);
        }
        $contentDecoded = json_decode($request->getContent(false), true);
        if (false === $contentDecoded || !isset($contentDecoded['sha'])) {
            throw new EmptyRepositoryException($repositoryName, $branchName);
        }
        return $contentDecoded['sha'];

    }
}
